

var R, G, B; R = G = B = 0.0; 


/*
		  V6----- V5
		 /|      /|
		V1------V0|
		| |     | |
		| |V7---|-|V4
		|/      |/
		V2------V3

*/


var VERTICES = new Float32Array ([

	//  X,  Y,  Z     NORMALS     U, V     R, G, B

	// FRONT FACE

	0.3, 0.3, 0.3,    0, 0, 1,    1, 1,    R, G, B,
     -0.3, 0.3, 0.3,    0, 0, 1,    0, 1,    R, G, B, 
     -0.3,-0.3, 0.3,    0, 0, 1,    0, 0,    R, G, B,
	0.3,-0.3, 0.3,    0, 0, 1,    1, 0,    R, G, B,

	// RIGHT FACE

	0.3, 0.3, 0.3,    1, 0, 0,    0, 1,    R, G, B,
	0.3,-0.3, 0.3,    1, 0, 0,    0, 0,    R, G, B,
	0.3,-0.3,-0.3,    1, 0, 0,    1, 0,    R, G, B,
	0.3, 0.3,-0.3,    1, 0, 0,    1, 1,    R, G, B,

	// TOP FACE

	0.3, 0.3, 0.3,    0, 1, 0,    1, 0,    R, G, B,
	0.3, 0.3,-0.3,    0, 1, 0,    1, 1,    R, G, B,
     -0.3, 0.3,-0.3,    0, 1, 0,    0, 1,    R, G, B,
     -0.3, 0.3, 0.3,    0, 1, 0,    0, 0,    R, G, B,

	// LEFT FACE

     -0.3, 0.3, 0.3,    -1, 0, 0,   1, 1,    R, G, B,
     -0.3, 0.3,-0.3,    -1, 0, 0,   0, 1,    R, G, B,
     -0.3,-0.3,-0.3,    -1, 0, 0,   0, 0,    R, G, B,
     -0.3,-0.3, 0.3,    -1, 0, 0,   1, 0,    R, G, B,

	// BOTTOM FACE

     -0.3,-0.3,-0.3,    0,-1, 0,    0, 0,    R, G, B,
	0.3,-0.3,-0.3,    0,-1, 0,    1, 0,    R, G, B,
	0.3,-0.3, 0.3,    0,-1, 0,    1, 1,    R, G, B,
     -0.3,-0.3, 0.3,    0,-1, 0,    0, 1,    R, G, B,

	// BACK FACE

	0.3,-0.3,-0.3,    0, 0,-1,    0, 0,    R, G, B,
     -0.3,-0.3,-0.3,    0, 0,-1,    1, 0,    R, G, B,
     -0.3, 0.3,-0.3,    0, 0,-1,    1, 1,    R, G, B,
	0.3, 0.3,-0.3,    0, 0,-1,    0, 1,    R, G, B

]);


var INDICES = new Uint16Array ([

	// FRONT

	0, 1, 2,
	0, 2, 3,
	
	//RIGHT

	4, 5, 6,
	4, 6, 7,
	
	// TOP

	8, 9, 10,
	8, 10, 11,
	
	// LEFT

	12, 13, 14,
	12, 14, 15,
	
	// BOTTOM

	16, 17, 18,
	16, 18, 19,
	
	// BACK

	20, 21, 22,
	20, 22, 23

]);


var webgl;


var KEYS_PRESSED = { FORWARD : false, BACKWARDS : false, LEFT: false, RIGHT: false };
var MOUSE_ROTATION = { PITCH: 0, YAW: 0 };
var MOUSE_COORDINATES = vec2(window.innerWidth / 2, window.innerHeight / 2);

// 1. The array that holds all the chunks.
// 2. A JavaScript object that holds the values, for the type of chunks that can be created.

var VOXEL_CHUNKS = [];
var CHUNK_TYPES = { NORMAL : 0, LIGHT : 1 }


var NUMBER_OF_LIGHT_CUBES = 0;



window.onkeydown = function( keyEvent ) {

	switch(keyEvent.keyCode) {
	
		case "W".charCodeAt(0):
			KEYS_PRESSED.FORWARD = true;
			break;
		
		case "A".charCodeAt(0):
			KEYS_PRESSED.LEFT = true;
			break;
		
		case "S".charCodeAt(0):
			KEYS_PRESSED.BACKWARDS = true;
			break;
		
		case "D".charCodeAt(0):
			KEYS_PRESSED.RIGHT = true;
			break;
		
		default:
	}
};


window.onkeyup = function( keyEvent ) {
	
	switch(keyEvent.keyCode) {
	
		case "W".charCodeAt(0):
			KEYS_PRESSED.FORWARD = false;
			break;
		
		case "A".charCodeAt(0):
			KEYS_PRESSED.LEFT = false;
			break;
		
		case "S".charCodeAt(0):
			KEYS_PRESSED.BACKWARDS = false;
			break;
		
		case "D".charCodeAt(0):
			KEYS_PRESSED.RIGHT = false;
			break;
		
		default:
	}
};



window.onmousemove =  function( mouseEvent ) {

	var X = 0, Y = 1;
	var coordinates = vec2();

	var speed = 0.15;


	coordinates[X] = mouseEvent.clientX;      
	coordinates[Y] = mouseEvent.clientY;


	MOUSE_ROTATION.YAW += (coordinates[X] - MOUSE_COORDINATES[X]) * speed;
	MOUSE_ROTATION.PITCH += (MOUSE_COORDINATES[Y] - coordinates[Y]) * speed;
	

	MOUSE_COORDINATES[X] = coordinates[X];
	MOUSE_COORDINATES[Y] = coordinates[Y];

	// When pitch is out of bounds, screen doesn't get flipped.
	
	if (MOUSE_ROTATION.PITCH > 89.0) MOUSE_ROTATION.PITCH = 89.0;
	if (MOUSE_ROTATION.PITCH < -89.0) MOUSE_ROTATION.PITCH = -89.0;
};



function Cube(x, y, z, r, g, b) {

	// Get a copy of the indices and vertices.
	
	var vertices = VERTICES.slice();
	var indices =  INDICES.slice(); 

	const increment = 11; // 0, 11, 22, 33, 44, 55, 66, 77, 88, 99, 110, 121, 132, 143, 154, 165, 176, 187, 198, 209, 220, 231, 242, 253;

	for (var i = 0; i < vertices.length; i += increment) {

		// Offset the cubes vertex positions based on the x, y, z, coordinates.

		vertices[i] += x;
		vertices[i + 1] += y;
		vertices[i + 2] += z;
		
		// Set the cubess vertex color based on the r, g, b vallues.

		vertices[i + 8] = r;
		vertices[i + 9] = g;
		vertices[i + 10] = b;
	}

	return { vertices, indices };
}


function Chunk(cubes, position, type, lightColor, lightintensity) {

	var indexOffset = 0;
	var vertexPositionSize = 72;

	var chunk = {};

		chunk.vertices = [];
		chunk.indices = [];

		chunk.type = 0;
		chunk.position = vec3();


	// Foreach individual cubes merge the vertices and idices together. 

	cubes.forEach((cube) => {
		
		chunk.vertices.push.apply(chunk.vertices, cube.vertices);

		cube.indices.forEach((index) => {

			chunk.indices.push( index + indexOffset );
		});

		indexOffset += vertexPositionSize  / 3;
	});


	chunk.vertices = new Float32Array(chunk.vertices);
	chunk.indices = new Uint16Array(chunk.indices);

	chunk.type = type;
	chunk.position = position;

	if(type === CHUNK_TYPES.LIGHT) {
		
		NUMBER_OF_LIGHT_CUBES++;
		
		chunk.lighColor = lightColor;
		chunk.lightintensity =  lightintensity;  
	}

	return chunk;
}


window.onload = function() {



	// Get reference to the canvas element, and enable either 'webgl' of 'experimental-webwebgl'.
	
	webgl = document.getElementById('canvas').getContext('webgl') 
			|| document.getElementById('canvas').getContext('experimental-webwebgl');

	if ( !webgl ) return; // The browser does not support WebGL.



	// Configure WebGL to enable the depth test and back face culling. 

	webgl.enable(webgl.DEPTH_TEST); webgl.enable(webgl.CULL_FACE);

	webgl.frontFace(webgl.CCW);
	webgl.cullFace(webgl.BACK);



	// Create a vertex buffer object and bind that buffer, to be the active array buffer. 

	var vertexBufferObject = webgl.createBuffer();
	webgl.bindBuffer(webgl.ARRAY_BUFFER, vertexBufferObject);
	webgl.bufferData(webgl.ARRAY_BUFFER, 11 * Float32Array.BYTES_PER_ELEMENT * 65536, webgl.DYNAMIC_DRAW);


	// Create a index buffer object and bind that buffer, to be the active element array buffer.

	var indexBufferObject = webgl.createBuffer();
	webgl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, indexBufferObject);
	webgl.bufferData(webgl.ELEMENT_ARRAY_BUFFER, Uint16Array.BYTES_PER_ELEMENT * 65536, webgl.DYNAMIC_DRAW);



	// Create a vertex shader for the light emitting cubes.

	var vertexShaderForLightCubes = webgl.createShader(webgl.VERTEX_SHADER);

	// Compile the vertex shader.

	webgl.shaderSource(vertexShaderForLightCubes, document.getElementById('vertexShaderLightCubes').text);
	webgl.compileShader(vertexShaderForLightCubes);

	// Validate the shader for any compile errors.

	if (!webgl.getShaderParameter(vertexShaderForLightCubes, webgl.COMPILE_STATUS)) {

		console.error(webgl.getShaderInfoLog(vertexShaderForLightCubes));
		return;
	}



	// Create a fragment shader for the light emitting cubes. 

	var fragmentShaderForLightCubes = webgl.createShader(webgl.FRAGMENT_SHADER);

	// Compile the fragment shader.

	webgl.shaderSource(fragmentShaderForLightCubes, document.getElementById('fragmentShaderLightCubes').text);
	webgl.compileShader(fragmentShaderForLightCubes);

	// Validate the shader for any compile errors.

	if (!webgl.getShaderParameter(fragmentShaderForLightCubes, webgl.COMPILE_STATUS)) {
		
		console.error(webgl.getShaderInfoLog(fragmentShaderForLightCubes));
		return;
	}



	// Create a program that will be used by the light emitting cubes.

	var programUsedByLightCubes = webgl.createProgram();

	// Attach the vertex and fragment shaders to the program.
	
	webgl.attachShader(programUsedByLightCubes, vertexShaderForLightCubes);
	webgl.attachShader(programUsedByLightCubes, fragmentShaderForLightCubes);

	// Link program.

	webgl.linkProgram(programUsedByLightCubes);

	if (!webgl.getProgramParameter(programUsedByLightCubes, webgl.LINK_STATUS)) {
		console.error(webgl.getProgramInfoLog(programUsedByLightCubes));
		return;
	}

	// Validate program.
	
	webgl.validateProgram(programUsedByLightCubes);
	
	if (!webgl.getProgramParameter(programUsedByLightCubes, webgl.VALIDATE_STATUS)) {
		console.error(webgl.getProgramInfoLog(programUsedByLightCubes));
		return;
	}


	var attrib_Position_Location = webgl.getAttribLocation(programUsedByLightCubes, 'attrib_position');
	
	webgl.enableVertexAttribArray(attrib_Position_Location);
	webgl.vertexAttribPointer(attrib_Position_Location, 3, webgl.FLOAT, webgl.FALSE, 11 * Float32Array.BYTES_PER_ELEMENT, 0);
	


	// Create a vertex shader for the normal cubes.

	var vertexShaderForNormalCubes = webgl.createShader(webgl.VERTEX_SHADER);
	
	// Compile the vertex shader.

	webgl.shaderSource(vertexShaderForNormalCubes, document.getElementById('vertexShaderNormalCubes').text);
	webgl.compileShader(vertexShaderForNormalCubes);

	// Validate the shader for any compile errors.

	if (!webgl.getShaderParameter(vertexShaderForNormalCubes, webgl.COMPILE_STATUS)) {
		
		console.error(webgl.getShaderInfoLog(vertexShaderForNormalCubes));
		return;
	}



	// Create a fragment shader for the normal cubes. 

	var fragmentShaderForNormalCubes = webgl.createShader(webgl.FRAGMENT_SHADER);

	// Compile the fragment shader.

	webgl.shaderSource(fragmentShaderForNormalCubes, document.getElementById('fragmentShaderNormalCubes').text);
	webgl.compileShader(fragmentShaderForNormalCubes);

	// Validate the shader for any compile errors.

	if (!webgl.getShaderParameter(fragmentShaderForNormalCubes, webgl.COMPILE_STATUS)) {
		
		console.error(webgl.getShaderInfoLog(fragmentShaderForNormalCubes));
		return;
	}



	// Create a program that will be used by the normal cubes.

	var programUsedByNormalCubes = webgl.createProgram();

	// Attach the vertex and fragment shaders to the program.
	
	webgl.attachShader(programUsedByNormalCubes, vertexShaderForNormalCubes);
	webgl.attachShader(programUsedByNormalCubes, fragmentShaderForNormalCubes);

	// Link program.

	webgl.linkProgram(programUsedByNormalCubes);

	if (!webgl.getProgramParameter(programUsedByNormalCubes, webgl.LINK_STATUS)) {
		console.error(webgl.getProgramInfoLog(programUsedByNormalCubes));
		return;
	}

	// Validate program.
	
	webgl.validateProgram(programUsedByNormalCubes);
	
	if (!webgl.getProgramParameter(programUsedByNormalCubes, webgl.VALIDATE_STATUS)) {
		console.error(webgl.getProgramInfoLog(programUsedByNormalCubes));
		return;
	}



	var attrib_Position_Location = webgl.getAttribLocation(programUsedByNormalCubes, 'attrib_position');
	
	webgl.enableVertexAttribArray(attrib_Position_Location);
	webgl.vertexAttribPointer(attrib_Position_Location, 3, webgl.FLOAT, webgl.FALSE, 11 * Float32Array.BYTES_PER_ELEMENT, 0);

	
	var attrib_Normal_Location = webgl.getAttribLocation(programUsedByNormalCubes, 'attrib_normal');
	
	webgl.enableVertexAttribArray(attrib_Normal_Location);
	webgl.vertexAttribPointer(attrib_Normal_Location, 3, webgl.FLOAT, webgl.true, 11 * Float32Array.BYTES_PER_ELEMENT, 3 * Float32Array.BYTES_PER_ELEMENT);


	var attrib_Color_Location = webgl.getAttribLocation(programUsedByNormalCubes, 'attrib_color');

	webgl.enableVertexAttribArray(attrib_Color_Location);
	webgl.vertexAttribPointer(attrib_Color_Location, 3, webgl.FLOAT, webgl.FALSE, 11 * Float32Array.BYTES_PER_ELEMENT, 8 * Float32Array.BYTES_PER_ELEMENT);



	editor(); // -> Create various chunks with voxel cubes in this function.



	// Setting up the model, view, projection, matrices for both the light emitting and normal cubes. 

	var modelMatrix = new Float32Array(16);
	var viewMatrix = new Float32Array(16);
	var projectionMatrix = new Float32Array(16);

	modelMatrix, viewMatrix = mat4();
	projectionMatrix = perspective(45, canvas.clientWidth / canvas.clientHeight, 0.1, 1000.0);



	// Get unifrom location from the shaders, that are used by the normal cubes.

	webgl.useProgram(programUsedByNormalCubes);



	// Model, view, projection, uniform locations.

	var uniform_model_location_normal = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_model');
	var uniform_view_location_normal = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_view');
	var uniform_projection_location_normal = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_projection');
	
	var uniform_viewPosition_location_normal = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_viewPosition');



	// Directional light uniform locations.

	var directionalLight_direction_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_directional_light.direction');
	var directionalLight_ambient_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_directional_light.ambient');
	var directionalLight_diffuse_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_directional_light.diffuse');
	var directionalLight_specular_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_directional_light.specular');



	// Point light uniform locations.
	
	var pointLights = [];

	for (var i = 0; i < NUMBER_OF_LIGHT_CUBES; i++) {

		var pointLight = { };

		pointLight.pointLights_position_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_point_lights[' + i + '].position');
		pointLight.pointLights_ambient_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_point_lights[' + i + '].ambient');
		pointLight.pointLights_diffuse_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_point_lights[' + i + '].diffuse');
		pointLight.pointLights_specular_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_point_lights[' + i + '].specular');
		pointLight.pointLights_constant_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_point_lights[' + i + '].constant');
		pointLight.pointLights_linear_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_point_lights[' + i + '].linear');
		pointLight.pointLights_quadratic_location = webgl.getUniformLocation(programUsedByNormalCubes, 'uniform_point_lights[' + i + '].quadratic');

		pointLights.push(pointLight);
	}



	// Get unifrom location from the shaders, that are used by the light cubes.

	webgl.useProgram(programUsedByLightCubes);



	// Model, view, projection, uniform locations.

	var uniform_model_location_light = webgl.getUniformLocation(programUsedByLightCubes, 'uniform_model');
	var uniform_view_location_light = webgl.getUniformLocation(programUsedByLightCubes, 'uniform_view');
	var uniform_projection_location_light = webgl.getUniformLocation(programUsedByLightCubes, 'uniform_projection');

	var uniform_lightColor_location = webgl.getUniformLocation(programUsedByLightCubes, 'uniform_lightColor');



	// Setting the unifroms for the shaders, that are used by the normal cubes.

	webgl.useProgram(programUsedByNormalCubes);


	// Setting the projection uniform.

	webgl.uniformMatrix4fv(uniform_projection_location_normal, webgl.FALSE, flatten(projectionMatrix));



	// Setting the directional light uniforms.

	webgl.uniform3fv(directionalLight_direction_location, vec3(0.0, -1.0, 0.0));
	webgl.uniform3fv(directionalLight_ambient_location, vec3(0.05, 0.05, 0.05));
	webgl.uniform3fv(directionalLight_diffuse_location, vec3(0.2, 0.2, 0.7));
	webgl.uniform3fv(directionalLight_specular_location, vec3(0.7, 0.7, 0.7));



	// Setting the point light uniforms.

	var j = 0;

	for (var i = 0; i < VOXEL_CHUNKS.length; i++) {

		if(VOXEL_CHUNKS[i].type === CHUNK_TYPES.LIGHT) {

			// Setting the position of the point light.

			webgl.uniform3fv(pointLights[j].pointLights_position_location, VOXEL_CHUNKS[i].position);
			

			// Setting the color of the point light.

			webgl.uniform3f(pointLights[j].pointLights_ambient_location,
				
				VOXEL_CHUNKS[i].lighColor[0] * VOXEL_CHUNKS[i].lightintensity, 
				VOXEL_CHUNKS[i].lighColor[1] * VOXEL_CHUNKS[i].lightintensity,  
				VOXEL_CHUNKS[i].lighColor[2] * VOXEL_CHUNKS[i].lightintensity
			);
			
			webgl.uniform3fv(pointLights[j].pointLights_diffuse_location, VOXEL_CHUNKS[i].lighColor);
			webgl.uniform3fv(pointLights[j].pointLights_specular_location, VOXEL_CHUNKS[i].lighColor);
			
			
			// Setting the color of the point light.
			

			webgl.uniform1f(pointLights[j].pointLights_constant_location, 1.0);
			webgl.uniform1f(pointLights[j].pointLights_linear_location, 0.14);
			webgl.uniform1f(pointLights[j].pointLights_quadratic_location, 0.07);

			j++;
		}
	}


	// Setting the unifroms for the shaders, that are used by the light cubes.

	webgl.useProgram(programUsedByLightCubes);

	webgl.uniformMatrix4fv(uniform_projection_location_light, webgl.FALSE, flatten(projectionMatrix));



	var previousFrame = 0.0;
	var speed = 0.0;

	// Setting up the initial EYE, AT, UP vectors. 

	var EYE = vec3(0, 0, -22), AT = vec3(0,0,1), UP = vec3(0, 1, 0);

	

	var loop = function( deltaTime ) {
		
		currentFrame = new Date().getTime();


		deltaTime = currentFrame - previousFrame; speed = 0.015 * deltaTime;



		// Determine which way is front. 
		
		var FRONT = vec3(
			
			Math.cos(radians(MOUSE_ROTATION.YAW)) * Math.cos(radians(MOUSE_ROTATION.PITCH)),
			Math.sin(radians(MOUSE_ROTATION.PITCH)),
			Math.sin(radians(MOUSE_ROTATION.YAW)) * Math.cos(radians(MOUSE_ROTATION.PITCH)),
		);

		AT = normalize(FRONT);



		// Move the position of the camera according to key inputs.

		if (KEYS_PRESSED.FORWARD) {

			EYE = add(EYE, scale(speed, AT));
		}

		if (KEYS_PRESSED.LEFT) {

			var RIGHT = normalize(cross(UP, AT));

			EYE = add(EYE, scale(speed, RIGHT));
		}

		if(KEYS_PRESSED.BACKWARDS) {

			EYE = subtract(EYE, scale(speed, AT));
		}

		if (KEYS_PRESSED.RIGHT) {
			
			var RIGHT = normalize(cross(UP, AT));

			EYE = subtract(EYE, scale(speed, RIGHT));
		}



		// Create the view matrix according to the EYE, AT UP vectors.
		
		viewMatrix = lookAt(EYE, add(EYE, AT), UP);



		// Set the view matrix for normal cubes.

		webgl.useProgram(programUsedByNormalCubes);

		webgl.uniformMatrix4fv(uniform_view_location_normal, webgl.FALSE, flatten(viewMatrix));
		webgl.uniform3f(uniform_viewPosition_location_normal, webgl.FALSE, EYE.x, EYE.y, EYE.z);



		// Set the view matrix for light cubes.

		webgl.useProgram(programUsedByLightCubes);

		webgl.uniformMatrix4fv(uniform_view_location_light, webgl.FALSE, flatten(viewMatrix));

		

		// Check whether the size of the canvas is the same size as the browser is displaying the canvas and set that accordingly.

		if (webgl.canvas.width  != Math.floor(webgl.canvas.clientWidth) 
			|| webgl.canvas.height != Math.floor(webgl.canvas.clientHeight)) {
			
			webgl.canvas.width  = Math.floor(webgl.canvas.clientWidth);
			webgl.canvas.height = Math.floor(webgl.canvas.clientHeight);
		}

		

		webgl.viewport(0, 0, webgl.canvas.width, webgl.canvas.height);
		webgl.clearColor(0.0, 0.0, 0.0, 1.0);
		webgl.clear(webgl.DEPTH_BUFFER_BIT | webgl.COLOR_BUFFER_BIT);



		// Render chunks.

		VOXEL_CHUNKS.forEach((chunk) => {

			
			switch(chunk.type) {
	
				case 0:

					// Setting the model matrix for the normal cubes based on the chunks position. 

					webgl.useProgram(programUsedByNormalCubes);
					
					modelMatrix = translate(chunk.position);
					webgl.uniformMatrix4fv(uniform_model_location_normal, webgl.FALSE, flatten(modelMatrix));
					
					break;
				
				case 1:

					// Setting the model matrix for the light cubes based on the chunks position. 

					webgl.useProgram(programUsedByLightCubes);

					modelMatrix = translate(chunk.position);
					webgl.uniformMatrix4fv(uniform_model_location_light, webgl.FALSE, flatten(modelMatrix));
					
					// Setting the color of the cube according to the light color.

					webgl.uniform3fv(uniform_lightColor_location, chunk.lighColor);

					break;

				default:
			}



			// Setting the vertex and the index buffer data based on the chunks vertices and indices.

			webgl.bindBuffer(webgl.ARRAY_BUFFER, vertexBufferObject);
			webgl.bufferSubData(webgl.ARRAY_BUFFER, 0, chunk.vertices);

			webgl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, indexBufferObject);
			webgl.bufferSubData(webgl.ELEMENT_ARRAY_BUFFER, 0, chunk.indices);

			

			webgl.drawElements(webgl.TRIANGLES, chunk.indices.length, webgl.UNSIGNED_SHORT, 0);
		});


		previousFrame = currentFrame;

		requestAnimationFrame(loop);
	};



	requestAnimationFrame(loop);
}



/*    EDITOR - CREATE VARIOUS CHUNKS WITH VOXEL CUBES.      */



function editor() {


	/* Light Cube Colors 
	
	1.0 1.0 1.0  - White

	1.0 0.0 0.0 - Red 
	0.0 1.0 0.0 - Green
	0.0 0.0 1.0 - Blue

	1.0 1.0 0.0 - Yellow
	1.0 0.0 1.0 - Purple
	0.0 1.0 1.0 - Cyan
	
	Light Cube Colors */

	var distance = 6.8; // n * 0.6

	for (var x = 0; x < 4; x++) {

		for (var z = 0; z < 4; z++) {

			for (var y = 0; y < 3; y++) {

				var randomNumber = Math.round(Math.random() * 2);
				
				switch(randomNumber) {
			
					case 0:
						var color = vec3(1.0, 0.0, 0.0);
						break;
						
					case 1:
						var color = vec3(0.0, 1.0, 0.0);
						break;
						
					case 2:
						var color = vec3(0.0, 0.0, 1.0);
						break;

					default:
				}
			
				createChunk(vec3( x * distance * Math.round(Math.random() * 3),  y * distance * Math.round(Math.random() * 3), z * distance * Math.round(Math.random() * 3)), color, color, 0.1, false);
			}
		}
	}
}


function createChunk(position, lightColor, chunkColor, intensity, enableRandom) {

	var VOXEL_NORMAL_CHUNK = [], VOXEL_LIGHT_CHUNK = [];
	
	var n = 3;

	for (var y = 0 ; y < n; y++) {

		for (var x = 0; x < n; x++) {

			var randomNumber = Math.round(Math.random() * (n - 1));

			for (var z = 0; z < n; z++) {

				if((x === 1) && z === randomNumber ) {

					VOXEL_LIGHT_CHUNK = [];
					VOXEL_LIGHT_CHUNK.push(Cube(x * 0.6, y * 0.6, z * 0.6, 0.0, 0.0, 0.0));
					VOXEL_CHUNKS.push(
						Chunk(VOXEL_LIGHT_CHUNK, position, CHUNK_TYPES.LIGHT, lightColor, intensity));			
				}
				else {

					if (!enableRandom) {

						var precision = 10;

						if(chunkColor[0] != 0.0) 
							var r = Math.floor(Math.random() * (1 * precision - 0 * precision) + 0 * precision) / (1*precision);

						if(chunkColor[1] != 0.0) 
							var g = Math.floor(Math.random() * (1 * precision - 0 * precision) + 0 * precision) / (1*precision);
						
						if(chunkColor[2] != 0.0) 
							var b = Math.floor(Math.random() * (1 * precision - 0 * precision) + 0 * precision) / (1*precision);
					
						VOXEL_NORMAL_CHUNK.push(
							Cube(x *0.6, y *0.6, z * 0.6, r, g, b));
					}

					else {
						VOXEL_NORMAL_CHUNK.push(
							Cube(x *0.6, y *0.6, z * 0.6 , Math.round(Math.random()), Math.round(Math.random()), 1.0));
					}
				}
			}
		}
	}

	VOXEL_CHUNKS.push(Chunk(VOXEL_NORMAL_CHUNK, position, CHUNK_TYPES.NORMAL));
}